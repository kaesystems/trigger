package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"time"

	pkg "bitbucket.org/kaesystems/trigger/pkg"
	"github.com/fatih/color"
	"gopkg.in/yaml.v2"
	"k8s.io/client-go/rest"
	ctrl "sigs.k8s.io/controller-runtime"
)

func Entrance() {

	green := color.New(color.FgGreen).SprintFunc()
	hello := "figlet -c trigger"

	cmd := exec.Command("/bin/bash", "-c", hello)
	stdout, err := cmd.Output()

	if err != nil {
		fmt.Println(green("trigger:v0.0.1"))
	} else {
		fmt.Println(green(string(stdout)))
		fmt.Println("-----------------------------------------------------------------------------")
	}

}

func main() {
	logger := log.Default()

	yellow := color.New(color.FgYellow).SprintFunc()
	green := color.New(color.FgGreen).SprintFunc()
	red := color.New(color.FgRed).SprintFunc()
	//cyan := color.New(color.FgCyan).SprintFunc()

	requests := []*pkg.Request{}
	activeRequests := []*pkg.Request{}

	Entrance()

	filename := "/var/lib/trigger/config.yaml"
	if len(os.Args) > 1 {
		filename = os.Args[1]
	}
	logger.Println(yellow("Configuration path: " + filename))

	err := ControlKubernetes()
	if err != nil {
		logger.Fatalln(red(err.Error()))
	}

	for {

		requestsBytes, err := ioutil.ReadFile(filename)
		if err != nil {
			logger.Println(red(err.Error()))
			time.Sleep(5 * time.Second)
			continue
		}

		err = yaml.Unmarshal(requestsBytes, &requests)
		if err != nil {
			logger.Println(red(err.Error()))
			time.Sleep(5 * time.Second)
			continue
		}

		if !AreRequestsEqual(requests, activeRequests) {
			starters, enders := ConfigureRoutines(requests, activeRequests)

			for k := range starters {
				err := starters[k].Validate()
				if err != nil {
					logger.Println(red(err.Error() + " -> " + starters[k].Name))
					continue
				}
				starters[k].Activity = pkg.ActivityAdded
				activeRequests = append(activeRequests, &starters[k])
			}

			for _, e := range enders {
				for k, req := range activeRequests {
					if req.Name == e.Name {
						logger.Println(yellow("Ending ", req.Name))
						activeRequests[k].Activity = pkg.ActivityEnded
						activeRequests = append(activeRequests[:k], activeRequests[k+1:]...)
					}
				}
			}

			for k, req := range activeRequests {
				if req.Activity == pkg.ActivityAdded {
					go func(request *pkg.Request) {
						logger.Println(green("Initializing ", request.Name))
						request.Activity = pkg.ActivityInitialized
						err := request.Send()
						if err != nil {
							logger.Println(red(err.Error() + " -> " + request.Name))

						}
					}(activeRequests[k])
				}
			}

		}

		time.Sleep(2 * time.Second)

	}

}

func AreRequestsEqual(
	r1 []*pkg.Request,
	r2 []*pkg.Request,
) bool {

	if len(r1) != len(r2) {
		return false
	}

	for _, i := range r1 {
		contains := false
		for _, j := range r2 {
			if i.Name == j.Name {
				contains = true
				continue
			}
		}

		if !contains {
			return false
		}
	}

	return true
}

func ConfigureRoutines(
	new []*pkg.Request,
	old []*pkg.Request,
) ([]pkg.Request, []pkg.Request) {

	starters := []pkg.Request{}
	enders := []pkg.Request{}

	for _, i := range new {
		if !IsContainingRequest(old, i) {
			starters = append(starters, *i)
		}
	}

	for _, i := range old {
		if !IsContainingRequest(new, i) {
			enders = append(enders, *i)
		}
	}

	return starters, enders

}

func IsContainingRequest(
	all []*pkg.Request,
	request *pkg.Request,
) bool {
	for _, v := range all {
		if v.Name == request.Name {
			return true
		}
	}

	return false
}

func ControlKubernetes() error {
	_, err := ctrl.GetConfig()
	if err != nil {
		_, err = rest.InClusterConfig()
	}

	return err
}
