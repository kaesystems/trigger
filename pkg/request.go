package request

import (
	"context"
	"errors"
	"io"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/fatih/color"

	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/rest"
	ctrl "sigs.k8s.io/controller-runtime"
)

type Method string

const (
	MethodGet  Method = "GET"
	MethodPost Method = "POST"
	MethodPut  Method = "PUT"
)

type Annotation struct {
	Group     string `yaml:"group"`
	Version   string `yaml:"version"`
	Resource  string `yaml:"resource"`
	Namespace string `yaml:"namespace"`
	Name      string `yaml:"name"`
}

type Activity string

const (
	ActivityAdded       Activity = "Added"
	ActivityInitialized Activity = "Initialized"
	ActivityEnded       Activity = "Ended"
)

type IRequest interface {
	Send() error
	Trigger() error
	AnnotateResource() error
}

type Request struct {
	Name        string      `yaml:"name"`
	Description string      `yaml:"description,omitempty"`
	URL         string      `yaml:"url"`
	Method      Method      `yaml:"method"`
	Frequency   string      `yaml:"frequency"`
	LastState   interface{} `yaml:"-"`
	Activity    Activity    `yaml:"activity,omitempty"`
	Annotation  Annotation  `yaml:"annotation"`
}

func (req *Request) Validate() error {
	if req.Name == "" {
		return errors.New("name field cannot be empty")
	}

	if req.URL == "" {
		return errors.New("url field cannot be empty")
	}

	if req.Method != MethodGet &&
		req.Method != MethodPost &&
		req.Method != MethodPut {
		return errors.New("method field can be GET, POST or PUT")
	}

	if req.Annotation.Group == "" {
		return errors.New("annotation.group field cannot be empty")
	}
	if req.Annotation.Version == "" {
		return errors.New("annotation.version field cannot be empty")
	}
	if req.Annotation.Resource == "" {
		return errors.New("annotation.resource field cannot be empty")
	}
	if req.Annotation.Namespace == "" {
		return errors.New("annotation.namespace field cannot be empty")
	}
	if req.Annotation.Name == "" {
		return errors.New("annotation.name field cannot be empty")
	}

	return nil
}

func (req *Request) Send() error {
	logger := log.Default()

	forAWhile, err := req.ParseFrequency()
	if err != nil {
		return err
	}

	magenta := color.New(color.FgMagenta).SprintFunc()
	red := color.New(color.FgRed).SprintFunc()
	cyan := color.New(color.FgCyan).SprintFunc()

	switch req.Method {
	case MethodGet:
		for {

			if req.Activity != ActivityEnded {

				resp, err := http.Get(req.URL)
				if err != nil {
					req.LastState = err
					logger.Println(cyan("Error at sending request: ", req.Name))
					time.Sleep(forAWhile)
					continue
				}
				defer resp.Body.Close()

				body, err := io.ReadAll(resp.Body)
				if err != nil {
					req.LastState = err
					logger.Println(cyan("Error at sending request: ", req.Name))
					time.Sleep(forAWhile)
					continue
				}
				bodyStr := string(body)

				if req.LastState == bodyStr {
					time.Sleep(forAWhile)
					continue
				} else {
					req.LastState = bodyStr
					err = req.Trigger()
					time.Sleep(forAWhile)
					if err != nil {
						continue
					}
				}
			} else {
				logger.Println(magenta("Closing ", req.Name))
				break
			}

		}
	case MethodPost:
		logger.Println(red("POST method is not supported yet"))
	case MethodPut:
		logger.Println(red("PUT method is not supported yet"))
	}

	return nil
}

func (req *Request) Trigger() error {
	logger := log.Default()

	red := color.New(color.FgRed).SprintFunc()
	cyan := color.New(color.FgCyan).SprintFunc()

	err := req.AnnotateResource()
	if err != nil {
		logger.Println(red("Cannot trigger: ", err.Error()))
	} else {
		logger.Println(cyan("Triggered successfully: ", req.Annotation.Resource))
	}

	return err

}

func (req *Request) AnnotateResource() error {

	config, err := ctrl.GetConfig()
	if err != nil {
		config, err = rest.InClusterConfig()
		if err != nil {
			return err
		}
	}
	k, err := dynamic.NewForConfig(config)
	if err != nil {
		return err
	}

	gvr := schema.GroupVersionResource{
		Group:    req.Annotation.Group,
		Version:  req.Annotation.Version,
		Resource: req.Annotation.Resource,
	}

	unstructured, err := k.Resource(gvr).
		Namespace(req.Annotation.Namespace).
		Get(
			context.Background(),
			req.Annotation.Name,
			v1.GetOptions{},
		)
	if err != nil {
		return err
	}

	annotations := unstructured.GetAnnotations()
	if annotations == nil {
		annotations = map[string]string{}
	}
	annotations["triggeredAt"] = time.Now().Format(time.RFC3339)
	unstructured.SetAnnotations(annotations)

	_, err = k.Resource(gvr).
		Namespace(req.Annotation.Namespace).
		Update(
			context.Background(),
			unstructured,
			v1.UpdateOptions{},
		)
	if err != nil {
		return err
	}

	return nil
}

type TimeLetter string

const (
	TimeLetterSecond TimeLetter = "s"
	TimeLetterMinute TimeLetter = "m"
	TimeLetterHour   TimeLetter = "h"
	TimeLetterDay    TimeLetter = "d"
)

func (req *Request) ParseFrequency() (time.Duration, error) {
	if req.Frequency == "" {
		return 10 * time.Second, nil
	}

	if strings.Contains(req.Frequency, string(TimeLetterSecond)) {
		letterIndex := strings.Index(req.Frequency, string(TimeLetterSecond))
		if letterIndex != len(req.Frequency)-1 {
			return 0, errors.New("invalid frequency input")
		}
		countStr := req.Frequency[0:letterIndex]
		countInt, err := strconv.Atoi(countStr)
		if err != nil {
			return 0, err
		}

		return time.Duration(countInt) * time.Second, nil

	} else if strings.Contains(req.Frequency, string(TimeLetterMinute)) {
		letterIndex := strings.Index(req.Frequency, string(TimeLetterMinute))
		if letterIndex != len(req.Frequency)-1 {
			return 0, errors.New("invalid frequency input")
		}
		countStr := req.Frequency[0:letterIndex]
		countInt, err := strconv.Atoi(countStr)
		if err != nil {
			return 0, err
		}

		return time.Duration(countInt) * time.Minute, nil

	} else if strings.Contains(req.Frequency, string(TimeLetterHour)) {
		letterIndex := strings.Index(req.Frequency, string(TimeLetterHour))
		if letterIndex != len(req.Frequency)-1 {
			return 0, errors.New("invalid frequency input")
		}
		countStr := req.Frequency[0:letterIndex]
		countInt, err := strconv.Atoi(countStr)
		if err != nil {
			return 0, err
		}

		return time.Duration(countInt) * time.Hour, nil

	} else if strings.Contains(req.Frequency, string(TimeLetterDay)) {
		letterIndex := strings.Index(req.Frequency, string(TimeLetterDay))
		if letterIndex != len(req.Frequency)-1 {
			return 0, errors.New("invalid frequency input")
		}
		countStr := req.Frequency[0:letterIndex]
		countInt, err := strconv.Atoi(countStr)
		if err != nil {
			return 0, err
		}

		return time.Duration(countInt) * 24 * time.Hour, nil
	} else {
		return 0, errors.New("invalid frequency input")
	}

}
