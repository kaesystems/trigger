FROM golang:1.17 as builder
SHELL [ "/bin/bash", "-c" ]
COPY . /app
WORKDIR /app
RUN go mod tidy
RUN go build

FROM ubuntu:focal
RUN apt update && apt install -y figlet
COPY --from=builder /app/trigger /usr/bin
COPY ./dynamic.yaml /var/lib/trigger/config.yaml